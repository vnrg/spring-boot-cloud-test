package com;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author vinicius-garcia
 *
 */
@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class CardServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CardServiceApplication.class, args);
    }
}

@Slf4j
@RestController
class CardService {

    /**
     * {@code eureka:instance:instance-id} do service.
     */
    @Value(value = "${eureka.instance.instance-id}")
    public String instanceId;

    @GetMapping(path = "/card", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<CardEnum>> getCardInformation() throws InterruptedException {
        log.debug(this.instanceId);
        return ResponseEntity.ok(Arrays.asList(CardEnum.values()));
    }

    @GetMapping(path = "/instance", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> getInstanceId() {
        log.info("Request para instance ID: " + this.instanceId);
        return ResponseEntity.ok(this.instanceId);
    }

}

enum CardEnum {

    VISA(1, "Visa"), MASTER(2, "Mastercard"), ELO(3, "Elo"), DINERS(4, "Diners");

    @Getter
    @Setter
    public int code;

    @Getter
    @Setter
    public String description;

    CardEnum(final int code, final String description) {
        this.code = code;
        this.description = description;
    }

}