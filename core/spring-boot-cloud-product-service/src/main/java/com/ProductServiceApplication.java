package com;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author vinicius-garcia
 *
 */
@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class ProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }
}

@Slf4j
@RestController
class CardService {

    /**
     * {@code eureka:instance:instance-id} do service.
     */
    @Value(value = "${eureka.instance.instance-id}")
    public String instanceId;

    @GetMapping(path = "/product", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<ProductEnum>> getProductInformation() {
        log.info(this.instanceId);
        return ResponseEntity.ok(Arrays.asList(ProductEnum.values()));
    }

    @GetMapping(path = "/instance", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> getInstanceId() {
        return ResponseEntity.ok(this.instanceId);
    }

}

enum ProductEnum {

    CREDITO(1), DEBITO(2), PARCELADO(3);

    @Getter
    @Setter
    public int code;

    ProductEnum(final int code) {
        this.code = code;
    }

}