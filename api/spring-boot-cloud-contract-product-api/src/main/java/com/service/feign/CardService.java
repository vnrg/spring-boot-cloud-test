/**
 * 
 */
package com.service.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author vinicius-garcia
 *
 */
@FeignClient("card-service")
public interface CardService {

    @GetMapping(value = "/card")
    public List<String> getCardInformation();

}
