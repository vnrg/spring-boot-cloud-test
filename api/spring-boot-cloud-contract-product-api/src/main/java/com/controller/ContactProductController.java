/**
 * 
 */
package com.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.service.feign.CardService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author vinicius-garcia
 *
 */
@Slf4j
@RestController
public class ContactProductController {

    @Autowired
    private transient CardService cardService;

    public ContactProductController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping(path = "/api/card", produces = { MediaType.APPLICATION_JSON_VALUE })
    @HystrixCommand(fallbackMethod = "getDefaultContractedProducts", commandProperties = {
            @HystrixProperty(name = "execution.isolation.strategy", value = "SEMAPHORE"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") })
    public ResponseEntity<List<String>> getContractedProducts() {
        return ResponseEntity.ok(this.cardService.getCardInformation());
    }

    @GetMapping(path = "/api/product", produces = { MediaType.APPLICATION_JSON_VALUE })
    @HystrixCommand(fallbackMethod = "getDefaultProduct")
    public ResponseEntity<List<String>> getProduct() {
        return ResponseEntity.ok(Arrays.asList("PARCELADO", "A VISTA"));
    }

    public ResponseEntity<List<String>> getDefaultProduct() {
        return ResponseEntity.ok(Arrays.asList("PARCELADO", "A VISTA"));
    }

    public ResponseEntity<List<String>> getDefaultContractedProducts() {
        log.debug("Hystrix in action!");
        return ResponseEntity.ok(Arrays.asList("CREDITO", "DEBITO"));
    }

}
