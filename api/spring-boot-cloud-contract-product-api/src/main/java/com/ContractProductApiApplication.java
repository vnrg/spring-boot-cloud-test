package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import com.service.feign.CardService;

@EnableHystrix
@EnableFeignClients(basePackageClasses = { CardService.class })
@EnableCircuitBreaker
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = "com.*")
public class ContractProductApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContractProductApiApplication.class, args);
    }
}
